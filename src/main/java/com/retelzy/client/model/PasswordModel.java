package com.retelzy.client.model;

import lombok.Data;

@Data
public class PasswordModel {

	private String email;
	private String oldPassword;
	private String newPassword;
	private String password;
	private String token;
}
