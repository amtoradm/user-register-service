package com.retelzy.client.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class UserModel {
	private String firstName;
	private String lastName;
	private String email;
	private String userName;
	private String password;
	private String roles;
	private String enabled;
}
