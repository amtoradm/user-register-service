package com.retelzy.client.entity;

import lombok.Data;

import java.util.Date;

import javax.persistence.*;

@Entity
@Data
public class User {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;
	private String firstName;
	private String lastName;
	private String email;

	@Column(length = 60)
	private String userName;

	@Column(length = 60)
	private String password;

	public String roles;
	private boolean enabled = false;
	private String modifyUser;
	private Date modifyTS;
}
