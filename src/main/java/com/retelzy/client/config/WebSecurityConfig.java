package com.retelzy.client.config;

import org.springframework.context.annotation.Bean;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;

@EnableWebSecurity
public class WebSecurityConfig extends WebSecurityConfigurerAdapter {

	private static final String[] WHITE_LIST_URLS = { "/hello", "/register", "/verifyRegistration*",
			"/resendVerifyToken*","/getUser","/deleteUser*","/enableOrisableUser*",
			"/sendMail", "/sendMailWithAttachment","/checkAccountAndSentResetPasswordLink",
			"/resetPassword","/savePassword","/verifyToken*","/changePassword"};

	@Bean
	public PasswordEncoder passwordEncoder() {
		return new BCryptPasswordEncoder(11);
	}

	@Override
	protected void configure(HttpSecurity http) throws Exception {
		http.csrf().disable().authorizeRequests().antMatchers(WHITE_LIST_URLS).permitAll().anyRequest().authenticated()
				.and().httpBasic();
	}
}
