package com.retelzy.client.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.retelzy.client.model.EmailDetails;
import com.retelzy.client.service.EmailService;

import lombok.extern.slf4j.Slf4j;
 
@CrossOrigin(origins = "*", maxAge = 3600)
@RestController
@Slf4j
public class EmailController {
 
    @Autowired 
    private EmailService emailService;
 
    // Sending a simple Email
	/*
	 * @PostMapping("/sendMail") public String sendMail(@RequestBody EmailDetails
	 * details) { String status = emailService.sendSimpleMail(details);
	 * 
	 * return status; }
	 */
 
    // Sending email with attachment
    @PostMapping("/sendMailWithAttachment")
    public String sendMailWithAttachment(
        @RequestBody EmailDetails details)
    {
        String status
            = emailService.sendMailWithAttachment(details);
 
        return status;
    }
}