package com.retelzy.client.controller;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import com.retelzy.client.entity.User;
import com.retelzy.client.entity.VerificationToken;
import com.retelzy.client.event.RegistrationCompleteEvent;
import com.retelzy.client.model.EmailDetails;
import com.retelzy.client.model.PasswordModel;
import com.retelzy.client.model.UserModel;
import com.retelzy.client.service.EmailService;
import com.retelzy.client.service.UserService;
import com.retelzy.client.repository.UserRepository;
import com.retelzy.client.response.MessageResponse;

import javax.servlet.http.HttpServletRequest;

import java.util.List;
import java.util.Optional;
import java.util.UUID;

@CrossOrigin(origins = "*", maxAge = 3600)
@RestController
@Slf4j
public class RegistrationController {

	@Autowired
	private UserService userService;

	@Autowired
	UserRepository userRepository;
	
	@Autowired 
	private EmailService emailService;

	@Autowired
	private ApplicationEventPublisher publisher;

	@PostMapping("/register")
	public ResponseEntity<?> registerUser(@RequestBody UserModel userModel, final HttpServletRequest request) {
		if (userRepository.existsByEmail(userModel.getEmail())) {
			return ResponseEntity.ok(new MessageResponse("Username is already taken!"));
		}
		User user = userService.registerUser(userModel);

		publisher.publishEvent(new RegistrationCompleteEvent(user, applicationUrl(request)));

		return ResponseEntity.ok(new MessageResponse("User registered successfully!"));
	}

	@GetMapping("/verifyRegistration")
	public ResponseEntity<?> verifyRegistration(@RequestParam("token") String token) {
		String result = userService.validateVerificationToken(token);
		if (result.equalsIgnoreCase("valid")) {
			return ResponseEntity.ok(new MessageResponse("User Verified successfully!"));
		}
		return ResponseEntity.badRequest().body(new MessageResponse("Error: Bad User!"));
	}
	
	@GetMapping(value = "/getUser")
	public ResponseEntity<?> getEmployee() {
		List<User> users = userService.getUsers();
		if (users != null) {
			return new ResponseEntity<>(users, HttpStatus.OK);
		} else {
			return new ResponseEntity<>(HttpStatus.NOT_FOUND);
		}
	}
	
	@PostMapping(value = "/deleteUser")
	public ResponseEntity<?> deleteUsers(@RequestBody UserModel userModel) {
		userService.deleteUsers(userModel.getEmail());
		return ResponseEntity.ok(new MessageResponse("User deleted successfully!"));
	}
	
	@PostMapping(value = "/enableOrisableUser")
	public ResponseEntity<?> enableOrDisableUsers(@RequestBody UserModel userModel) {
		userService.enableOrDisableUsers(userModel.getEmail(),userModel.getEnabled());
		if(userModel.getEnabled()=="true") {
			return ResponseEntity.ok(new MessageResponse("User Account Inactive Successfully!"));
		}else {
			return ResponseEntity.ok(new MessageResponse("User Account Active Successfully!"));
		}
	}


	@GetMapping("/resendVerifyToken")
	public String resendVerificationToken(@RequestParam("token") String oldToken, HttpServletRequest request) {
		VerificationToken verificationToken = userService.generateNewVerificationToken(oldToken);
		User user = verificationToken.getUser();
		resendVerificationTokenMail(user, applicationUrl(request), verificationToken);
		return "Verification Link Sent";
	}

	@PostMapping("/resetPassword")
	public String resetPassword(@RequestBody PasswordModel passwordModel, HttpServletRequest request) {
		User user = userService.findUserByEmail(passwordModel.getEmail());
		String url = "";
		if (user != null) {
			String token = UUID.randomUUID().toString();
			userService.createPasswordResetTokenForUser(user, token);
			url = passwordResetTokenMail(user, applicationUrl(request), token);
		}
		return url;
	}

	private String passwordResetTokenMail(User user, String applicationUrl, String token) {
		String url = applicationUrl + "/savePassword?token=" + token;

		// sendVerificationEmail()
		log.info("Click the link to Reset your Password: {}", url);
		return url;
	}

	private void resendVerificationTokenMail(User user, String applicationUrl, VerificationToken verificationToken) {
		String url = applicationUrl + "/verifyRegistration?token=" + verificationToken.getToken();

		// sendVerificationEmail()
		log.info("Click the link to verify your account: {}", url);
	}

	private String applicationUrl(HttpServletRequest request) {
		return "http://" + request.getServerName() + ":" + request.getServerPort() + request.getContextPath();
	}
	
	@GetMapping(value = "/checkAccountAndSentResetPasswordLink")
	public ResponseEntity<?> checkAccount(@RequestParam("email") String email) {
		User user = userRepository.getUserInfo(email);
		
		if (user != null) {
			EmailDetails details = new EmailDetails();
			
			String token = UUID.randomUUID().toString();
			userService.createPasswordResetTokenForUser(user, token);
			
			details.setRecipient(email);
			String status = emailService.sendSimpleMail(details, token);
			return new ResponseEntity<>(user, HttpStatus.OK);
		} else {
			return new ResponseEntity<>(HttpStatus.NOT_FOUND);
		}
	}
	
	@GetMapping("/verifyToken")
	public ResponseEntity<?> VerifyToken(@RequestParam("token") String token) {
		String result = userService.validatePasswordResetToken(token);
		
		if(result.equalsIgnoreCase("valid")) {
			return ResponseEntity.ok(new MessageResponse("valid"));
		}else {
			return new ResponseEntity<>(HttpStatus.NOT_FOUND);
		}
		
	}
	
	@PostMapping("/savePassword")
	public ResponseEntity<?> savePassword( @RequestBody PasswordModel passwordModel) {

		Optional<User> user = userService.getUserByPasswordResetToken(passwordModel.getToken());
		if (user.isPresent()) {
			userService.changePassword(user.get(), passwordModel.getPassword());
			return ResponseEntity.ok(new MessageResponse("Password Reset Successfully"));
		} else {
			return new ResponseEntity<>(HttpStatus.NOT_FOUND);
		} 
	}
	
	@PostMapping("/changePassword")
	public ResponseEntity<?> changePassword(@RequestBody PasswordModel passwordModel) {
		User user = userRepository.getUserInfo(passwordModel.getEmail());
		if (!userService.checkIfValidOldPassword(user, passwordModel.getOldPassword())) {
			return new ResponseEntity<>(HttpStatus.NOT_FOUND);
		}
		// Save New Password
		userService.changePassword(user, passwordModel.getNewPassword());
		return ResponseEntity.ok(new MessageResponse("Password Changed Successful !!"));
	}
	
}
