package com.retelzy.client.service;

import java.util.List;
import java.util.Optional;

import com.retelzy.client.entity.User;
import com.retelzy.client.entity.VerificationToken;
import com.retelzy.client.model.UserModel;

public interface UserService {
	User registerUser(UserModel userModel);

	void saveVerificationTokenForUser(String token, User user);

	String validateVerificationToken(String token);

	VerificationToken generateNewVerificationToken(String oldToken);

	User findUserByEmail(String email);

	void createPasswordResetTokenForUser(User user, String token);

	String validatePasswordResetToken(String token);

	Optional<User> getUserByPasswordResetToken(String token);

	void changePassword(User user, String newPassword);

	boolean checkIfValidOldPassword(User user, String oldPassword);

	List<User> getUsers();

	void deleteUsers(String string);

	void enableOrDisableUsers(String email, String enabled);
}
