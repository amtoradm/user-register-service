package com.retelzy.client.service;

import com.retelzy.client.model.EmailDetails;

// Interface
public interface EmailService {
 
    // Method
    // To send a simple email
    String sendSimpleMail(EmailDetails details, String token);
 
    // Method
    // To send an email with attachment
    String sendMailWithAttachment(EmailDetails details);
}