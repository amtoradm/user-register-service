package com.retelzy.client.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.retelzy.client.entity.PasswordResetToken;

@Repository
public interface PasswordResetTokenRepository extends JpaRepository<PasswordResetToken, Long> {
	
	@Query(value = "select * from password_reset_token where token=?1", nativeQuery = true)
	PasswordResetToken findByToken(String token);
}
