package com.retelzy.client.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.retelzy.client.entity.User;

@Repository
public interface UserRepository extends JpaRepository<User, Long> {

	Boolean existsByEmail(String username);

	User findByEmail(String email);

	@Query(value = "select * from user", nativeQuery = true)
	List<User> getUsers();
	
	@Query(value = "select * from user where email=?1", nativeQuery = true)
	User getUserInfo(String email);

	@Modifying
	@Transactional
	@Query(value = "delete from user where email=?1", nativeQuery = true)
	void deleteUser(String email);

	@Modifying
	@Transactional
	@Query(value = "update user set enabled=?1 where email=?2", nativeQuery = true)
	void enableOrDisableUsers(Boolean enabled, String email);

}
